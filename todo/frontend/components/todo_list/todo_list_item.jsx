import React from 'react';
import TodoDetailView from './todo_detail_view';
import TodoDetailViewContainer from './todo_detail_view_container';


class TodoListItem extends React.Component {
  constructor(props) {
    super(props);
    // this.handleDelete = this.handleDelete.bind(this);
    this.handleDone = this.handleDone.bind(this);
    this.state = {
      detail: false
    };
    this.handleDetail = this.handleDetail.bind(this);
  }

  // handleDelete(e) {
  //   e.preventDefault();
  //   return this.props.removeTodo(this.props.todo.id);
  // }

  handleDone(e) {
    e.preventDefault();
    return this.props.updateTodo(this.props.todo);
  }

  handleDetail(e) {
    this.setState({detail: !this.state.detail});
  }

  render() {
    return (
      <li>
        <h3 onClick={this.handleDetail}>Title: {this.props.todo.title}</h3>
        <button onClick = {this.handleDone}>{this.props.todo.done ? "Undo" : "Done"}</button>
        <TodoDetailViewContainer todo = {this.props.todo} detail= {this.state.detail}/>
      </li>
    );
  }
}

export default TodoListItem;
