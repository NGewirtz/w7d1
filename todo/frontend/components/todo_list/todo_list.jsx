import React from 'react';
import TodoListItem from './todo_list_item';
import TodoForm from './todo_form';

const TodoList = (props) => {
  const listItems = props.todos.map((el, idx) => (<TodoListItem
     key={el.id}
     todo={el}
     updateTodo={props.updateTodo}
     removeTodo={props.removeTodo}/>));
  return (
    <div>
      <ul>
        {listItems}
      </ul>
      <TodoForm receiveTodo={props.receiveTodo} />
    </div>

  );
};


export default TodoList;
