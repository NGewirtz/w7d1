import React from 'react';

class TodoDetailView extends React.Component {

  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(e) {
    e.preventDefault();
    return this.props.removeTodo(this.props.todo.id);
  }

  render() {
    return (
      <div>
        <p>{this.props.detail && this.props.todo.body}</p>
        <button onClick = {this.handleDelete}>Remove This Item</button>
      </div>
    );
  }


}

export default TodoDetailView;
