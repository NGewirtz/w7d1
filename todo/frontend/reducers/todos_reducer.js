import { RECEIVE_TODOS, RECEIVE_TODO, REMOVE_TODO, UPDATE_TODO } from '../actions/todo_actions';



const initialState = {
  1: {
    id: 1,
    title: 'wash car',
    body: 'with soap',
    done: false
  },
  2: {
    id: 2,
    title: 'wash dog',
    body: 'with shampoo',
    done: true
  },
};

const todosReducer = (state = initialState, action) => {
  switch(action.type) {
    case RECEIVE_TODOS: {
      const newObj = {};
      action.todos.forEach((el) => {
        newObj[el.id] = el;
      });
      return newObj;
    }
    case RECEIVE_TODO: {
      const newObj = Object.assign({}, state, { [action.todo.id]: action.todo });
      return newObj;
    }
    case REMOVE_TODO: {
      const newObj = Object.assign({}, state);
      delete newObj[action.id];
      return newObj;
    }
    case UPDATE_TODO: {
      const newState = Object.assign({}, state);
      newState[action.todo.id].done = !newState[action.todo.id].done;
      return newState;
    }
    default: {
      return state;
    }
  }
};

export default todosReducer;
