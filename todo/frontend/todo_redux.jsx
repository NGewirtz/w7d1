import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/store.js';
import { receiveTodo } from './actions/todo_actions';
import { receiveTodos } from './actions/todo_actions';
import Root from './components/root';
import allTodos from './reducers/selectors';

const store = configureStore();

document.addEventListener('DOMContentLoaded', () => {
  window.store = store;
  window.receiveTodo = receiveTodo;
  window.receiveTodos = receiveTodos;
  window.allTodos = allTodos;
  const main = document.querySelector('main');
  ReactDOM.render(<Root store={store} />, main);
});
